package me.benfah.gravestone.block.instance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.benfah.cu.api.BlockInstance;
import me.benfah.gravestone.util.BukkitSerialization;

public class BlockGraveInstance extends BlockInstance
{
	
	String serializedInv = "";
	String playername = "null";
	
	
	public String getPlayername()
	{
		return playername;
	}

	public void setPlayername(String playername)
	{
		this.playername = playername;
	}

	public BlockGraveInstance(Block b)
	{
		super(b);
	}
	
	@Override
	public void read()
	{
		if(hasMetadataValue("stacks"))
		{
			if(getMetadataValue("stacks") instanceof String)
			serializedInv = (String) getMetadataValue("stacks");
			else
			{
				ArrayList<Map<String, Object>> arraylist = (ArrayList<Map<String, Object>>) getMetadataValue("stacks");
				Inventory inv = Bukkit.createInventory(null, 36);
				for(Map<String, Object> m : arraylist)
				{
					inv.addItem(ItemStack.deserialize(m));
				}
				serializedInv = BukkitSerialization.toBase64(inv);
			}
		}
			
		
		if(hasMetadataValue("player"))
		playername = (String) getMetadataValue("player");
		
		
	}
	@Override
	public void write()
	{
//		ArrayList<Map<String, Object>> serialized = new ArrayList<>();
		
		setMetadataValue("stacks", serializedInv);
		setMetadataValue("player", playername);

		
	}
	
	
	public void setDrops(String s)
	{
		serializedInv = s;
	}
	public ArrayList<ItemStack> getDrops()
	{
		
		ItemStack[] s = new ItemStack[]{};
		try {
			if(!serializedInv.isEmpty())
			s = BukkitSerialization.fromBase64(serializedInv).getContents();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		ArrayList<ItemStack> al = new ArrayList<>();
		for(ItemStack is : s)
		al.add(is);	
		
		return al;
	}
}
