package me.benfah.gravestone.block;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import me.benfah.cu.api.BlockInstance;
import me.benfah.cu.api.CustomBlock;
import me.benfah.cu.api.IInstanceProvider;
import me.benfah.gravestone.block.instance.BlockGraveInstance;

public class BlockGraveStone extends CustomBlock implements IInstanceProvider
{

	public BlockGraveStone(String name, String modelPath, String title)
	{
		super(name, modelPath, title);
	}

	@Override
	public Class<?> getInstanceName()
	{
		return BlockGraveInstance.class;
	}
	
	@Override
	public void onInteract(PlayerInteractEvent e)
	{
		if(e.getHand() == EquipmentSlot.HAND)
		{	
		BlockGraveInstance bgi = (BlockGraveInstance) BlockInstance.getBlockInstance(e.getClickedBlock());
		e.getPlayer().sendMessage(ChatColor.RED + "Here lies " + bgi.getPlayername());
		}
		super.onInteract(e);
	}
	
	
	@Override
	public ItemStack[] getLoot(Block b)
	{
		BlockGraveInstance bgi = (BlockGraveInstance) BlockInstance.getBlockInstance(b);
		return bgi.getDrops().toArray(new ItemStack[bgi.getDrops().size()]);
	}
	
	
}
