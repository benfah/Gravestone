package me.benfah.gravestone.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.benfah.cu.api.CustomRegistry;
import me.benfah.gravestone.block.BlockGraveStone;
import me.benfah.gravestone.listener.ExplosionListener;
import me.benfah.gravestone.listener.PlayerDeathListener;

public class GraveStone extends JavaPlugin
{
	
	public static BlockGraveStone blockGraveStone;
	
	@Override
	public void onEnable()
	{
		blockGraveStone = new BlockGraveStone("gravestone", "block/gravestone", "Gravestone");
		
		Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new ExplosionListener(), this);
		CustomRegistry.registerBlock(blockGraveStone, this);

		super.onEnable();
	}
}
