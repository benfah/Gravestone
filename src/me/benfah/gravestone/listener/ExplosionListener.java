package me.benfah.gravestone.listener;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import me.benfah.cu.api.CustomRegistry;
import me.benfah.gravestone.main.GraveStone;

public class ExplosionListener implements Listener
{
	@EventHandler
	public void onExplode(EntityExplodeEvent e)
	{
		for(Block b : e.blockList())
		{
			if(CustomRegistry.isCustomBlock(b))
			{
				if(CustomRegistry.getCustomBlockByBlock(b).getName().equals(GraveStone.blockGraveStone.getName()))
				{
					e.blockList().remove(b);
				}
			}
		}
	}
}
