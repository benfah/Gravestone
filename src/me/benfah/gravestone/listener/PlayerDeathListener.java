package me.benfah.gravestone.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.benfah.cu.api.BlockInstance;
import me.benfah.gravestone.block.instance.BlockGraveInstance;
import me.benfah.gravestone.main.GraveStone;
import me.benfah.gravestone.util.BukkitSerialization;

public class PlayerDeathListener implements Listener
{
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
	    if(e.getEntity().getLocation().getBlock().getType().isTransparent() || !e.getEntity().getLocation().getBlock().getType().isSolid())
	    {
	    	GraveStone.blockGraveStone.setBlock(e.getEntity().getLocation());
	    	BlockGraveInstance bgi = (BlockGraveInstance) BlockInstance.getBlockInstance(e.getEntity().getLocation().getBlock());
	    	
	    	
	    	ArrayList<ItemStack> clone = (ArrayList<ItemStack>) ((ArrayList<ItemStack>)e.getDrops()).clone();
	    	
	    	String serialized = BukkitSerialization.toBase64(e.getEntity().getInventory());
	    	
	    	e.getDrops().clear();

	    	Bukkit.getScheduler().runTaskLaterAsynchronously(GraveStone.getPlugin(GraveStone.class),  new Runnable() {
				
				@Override
				public void run()
				{
			    	bgi.setDrops(serialized);
			    	bgi.setPlayername(e.getEntity().getName());
				}
			}, 1);
	    }
	}
	
}
